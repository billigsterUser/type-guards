
[![pipeline status][PipelineStatusBadge]][ProjectUrl]
[![coverage report][CoverageBadge]][ProjectUrl]

# type-guards

## [Wiki](https://billigsterUser.gitlab.io/type-guards/)



## Targeting **ESnext** - Tested with node **>=** v**15.3.0**
because i ❤ [Optional chaining](https://2ality.com/2019/07/optional-chaining.html)

## Installing from source
### Linux, MacOS, Windows, *BSD, Solaris, WSL, Android, Raspbian

```bash
git clone https://gitlab.com/billigsterUser/type-guards.git
cd type-guards
npm i
nm test

```

### Install

```bash
echo '# Set URL for your scoped packages.
# For example package with name `@foo/bar` will use this URL for download
@billigsterUser:registry=https://gitlab.com/api/v4/packages/npm/
@billigsteruser:registry=https://gitlab.com/api/v4/packages/npm/' >> .npmrc

npm i @billigsteruser/type-guards
```

### Usage

```typescript
import { isArr, isFunc, isKeyof, isObj, isStr, isUndef } from '@billigsteruser/type-guards/dist/core/typeGuards'
// or 
import { isArr, isFunc, isKeyof, isObj, isStr, isUndef } from '@billigsteruser/type-guards'
// or 
import { typeGuards } from '@billigsteruser/type-guards'

const testA='a'

console.log(isArr(testA)) // false
console.log(typeGuards.isArr(testA)) // false
```

That's it!


[PipelineStatusBadge]: https://gitlab.com/billigsterUser/type-guards/badges/main/pipeline.svg
[CoverageBadge]: https://gitlab.com/billigsterUser/type-guards/badges/main/coverage.svg
[ProjectUrl]: https://gitlab.com/billigsterUser/type-guards/commits/main
[ProjectPage]: https://billigsterUser.gitlab.io/type-guards/
