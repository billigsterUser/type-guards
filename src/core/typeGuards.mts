/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/ban-types */


export const checkProp = <T extends object, TKey extends PropertyKey>(obj: T, prop: TKey): obj is T & { [J in TKey]: unknown } =>
	isKeyof(obj, prop) && hasProps(obj, prop) && !!obj?.[prop]

export const checkProps = <T extends object, TKey extends PropertyKey>(obj: T, ...props: TKey[]) => {
	for (const prop of props) {
		if (!checkProp(obj, prop)) { return false }
		(obj as unknown) = obj[prop]
		if (props[props.length - 1] === prop) { return obj }
	}
	return false
}
export const isKeyof = <T extends object>(o: T, k: PropertyKey): k is keyof T => k in o

interface TypeMap { // for mapping from strings to types
	array: unknown[]
	boolean: boolean
	number: number
	string: string
}
export type Parser<T> = (value: unknown) => T | null

export function hasProps<T, TKey extends PropertyKey>(
	obj: T,
	...keys: TKey[]
): obj is T & { [J in TKey]: unknown } {
	// return !!obj && keys.every(key => Object.prototype.hasOwnProperty.call(obj, key))
	// eslint-disable-next-line no-prototype-builtins
	return !!obj && isObj(obj) && keys.every(key => obj.hasOwnProperty(key))
}


export const createTypeGuard = <T, >(parse: Parser<T>) => (v: unknown): v is T => parse(v) !== null
export interface PlainObject {

	// Object.hasOwn() is intended as a replacement for Object.hasOwnProperty(). See https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/hasOwn
	hasOwn<TKey extends PropertyKey>(key: TKey): this is Record<TKey, unknown>
	hasOwnProperty<TKey extends PropertyKey>(key: TKey): this is Record<TKey, unknown>
	// hasProperties<T extends object, TKey extends PropertyKey>( obj: T, ...keys: TKey[] ): obj is T & { [J in TKey]: unknown }
}
export function isPlainObject(v: unknown): v is PlainObject { return isObj(v) }


// tslint:disable-next-line: ban-types
export type ObjectType<T> = (new (...args: unknown[]) => T) | Function
type PrimitiveOrConstructor = // 'string' | 'number' | 'boolean' | constructor
	| (new (...args: unknown[]) => unknown)
	| keyof TypeMap

// infer the guarded type from a specific case of PrimitiveOrConstructor
type GuardedType<T extends PrimitiveOrConstructor> = T extends new (...args: unknown[]) => infer U ? U : T extends keyof TypeMap ? TypeMap[T] : never
// finally, guard ALL the types!
export function typeGuard<T extends PrimitiveOrConstructor>(o: unknown, className: T):
	o is GuardedType<T> {
	const localPrimitiveOrConstructor: PrimitiveOrConstructor = className
	if (typeof localPrimitiveOrConstructor === 'string') {
		return typeof o === localPrimitiveOrConstructor
	}
	return o instanceof localPrimitiveOrConstructor
}


export const isEvt = <T, >(v: unknown, e: ObjectType<T>) => v instanceof e
export const isArr = (v: unknown): v is unknown[] => !!((v && Array.isArray(v)) || (v && typeof v === 'object' && v.constructor === Array))
export const isBool = (v: unknown): v is boolean => typeof v === 'boolean'
// eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
export const isErr = (v: any): v is Error => !!(v instanceof Error || (v && v.__proto__ && v.__proto__.name === 'Error'))
// tslint:disable-next-line: ban-types
export const isFunc = (v: unknown): v is Function => typeof v === 'function'
export const isNull = (v: unknown): v is null => v === null
export const isNum = (v: unknown): v is number => typeof v === 'number' && !isNaN(v) && isFinite(v)
/**
	 * checks if v is true-ish and an Object
	 *
	 * so null is an Object but false-ish so this func will return false for isObj(null)
	 *
	 * *typeGuard*
	 * @param {unknown} v
	 * @return {boolean}  {v is object}
	 */
export const isObj = (v: unknown): v is object => !!v && typeof v === 'object' && v instanceof Object /*&& !Array.isArray(v) v.constructor === Object */
export const isStr = (v: unknown): v is string => typeof v === 'string' || v instanceof String
export const isUndef = (v: unknown): v is undefined => typeof v === 'undefined'
export const isObjAndInstance = <T, >(v: unknown, type: ObjectType<T>): v is typeof type => !!(v && v instanceof type)

export function isByteArr(v: unknown): v is Uint8Array { return v instanceof Uint8Array }
export function isNullOrUndef(v: unknown): v is null | undefined {
	return isNull(v) || isUndef(v)
}
export function isNonNullish<T>(v: T | null | undefined): v is NonNullable<T> {
	return !isNullOrUndef(v)
}
export function assertNonNullish<T>(v: T, msg: string): asserts v is NonNullable<T> {
	if (v === null || v === undefined) { throw Error(msg) }
}

