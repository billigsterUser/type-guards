import { isObj } from '../src/index.mjs'

describe('test typeGuards', () => {
	class Foo { constructor(public bar: string) { this.bar = bar } }
	// eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
	// function Bar(this: any) { this.bar = '' }
	test('isObj Object Literal', () => {
		const o = { bar: '' }
		const isobj = isObj(o)
		expect(isobj).toBe(true)
	})
	test('isObj Instance Object', () => {
		const o = new Foo('')
		const isobj = isObj(o)
		expect(isobj).toBe(true)
	})
	it('should be true when the value is an object.', () => {
		expect(isObj({})).toBe(true)
		expect(isObj(Object.create({}))).toBe(true)
		expect(isObj(Object.create(Object.prototype))).toBe(true)
		expect(isObj(/foo/)).toBe(true)

		expect(isObj([])).toBe(true)
		expect(isObj(['foo', 'bar'])).toBe(true)

		// expect(isObj(new Bar())).toBe(true)
	})

	it('should be false when the value is not an object.', () => {
		expect(!isObj('whatever')).toBe(true)
		expect(!isObj(1)).toBe(true)
		expect(!isObj(() => { })).toBe(true)
		// eslint-disable-next-line @typescript-eslint/ban-ts-comment
		// @ts-expect-error
		expect(!isObj()).toBe(true)
		expect(!isObj(undefined)).toBe(true)
		expect(!isObj(null)).toBe(true)
		expect(!isObj(Object.create(null))).toBe(true)
	})
})